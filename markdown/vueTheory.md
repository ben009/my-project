## vue原理之响应式原理及实现
#### 什么是响应式Reactivity
Reactivity表示一个状态改变之后，如何动态改变整个系统，在实际项目应用场景中即数据如何动态改变Dom。
#### 需求
现在有一个需求，有a和b两个变量，要求b一直是a的10倍，怎么做？
#### 简单尝试
```js
let a = 3;
let b = a * 10;
console.log(b); // 30
``` 
乍一看好像满足要求，但此时b的值是固定的，不管怎么修改a，b并不会跟着一起改变。也就是说b并没有和a保持数据上的同步。只有在a变化之后重新定义b的值，b才会变化。
#### 简单尝试2
将a和b的关系定义在函数内，那么在改变a之后执行这个函数，b的值就会改变。伪代码如下。
```js
onAChanged(() => {
    b = a * 10;
})
```
所以现在的问题就变成了如何实现onAChanged函数，当a改变之后自动执行onAChanged，请看后续。
#### 结合view层
现在把a、b和view页面相结合，此时a对应于数据，b对应于页面。业务场景很简单，改变数据a之后就改变页面b
```js
<span class="cell b"></span>

document
    .querySelector('.cell.b')
    .textContent = state.a * 10
```
现在建立数据a和页面b的关系，用函数包裹之后建立以下关系。
```js
<span class="cell b"></span>

onStateChanged(() => {
    document
        .querySelector(‘.cell.b’)
        .textContent = state.a * 10
})
```
再次抽象之后如下所示。
```js
<span class="cell b">
    {{ state.a * 10 }}
</span>

onStateChanged(() => {
    view = render(state)
})
```
view = render(state)是所有的页面渲染的高级抽象。这里暂不考虑view = render(state)的实现，因为需要涉及到DOM结构及其实现等一系列技术细节。这边需要的是onStateChanged的实现。
#### 实现
实现方式是通过Object.defineProperty中的getter和setter方法。具体使用方法参考如下链接。
>[MDN之Object.defineProperty](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Object/defineProperty)
需要注意的是get和set函数是存取描述符，value和writable函数是数据描述符。描述符必须是这两种形式之一，但二者不能共存，不然会出现异常。
#### 实例1：实现convert()函数
要求如下：
* 1、传入对象obj作为参数
* 2、使用Object.defineProperty转换对象的所有属性
* 3、转换后的对象保留原始行为，但在get或者set操作中输出日志 

示例：
```js
const obj = { foo: 123 }
convert(obj)


obj.foo // 输出 getting key "foo": 123
obj.foo = 234 // 输出 setting key "foo" to 234
obj.foo // 输出 getting key "foo": 234
```
在了解Object.defineProperty中getter和setter的使用方法之后，通过修改get和set函数就可以实现onAChanged和onStateChanged。
实现：
```js
function convert (obj) {

  // 迭代对象的所有属性
  // 并使用Object.defineProperty()转换成getter/setters
  Object.keys(obj).forEach(key => {
  
    // 保存原始值
    let internalValue = obj[key]
    
    Object.defineProperty(obj, key, {
      get () {
        console.log(`getting key "${key}": ${internalValue}`)
        return internalValue
      },
      set (newValue) {
        console.log(`setting key "${key}" to: ${newValue}`)
        internalValue = newValue
      }
    })
  })
}
```
#### 实例2：实现Dep类
要求如下：
* 1、创建一个Dep类，包含两个方法：depend和notify
* 2、创建一个autorun函数，传入一个update函数作为参数
* 3、在update函数中调用dep.depend()，显式依赖于Dep实例
* 4、调用dep.notify()触发update函数重新运行 

示例：
```js
const dep = new Dep()

autorun(() => {
  dep.depend()
  console.log('updated')
})
// 注册订阅者，输出 updated

dep.notify()
// 通知改变，输出 updated
```
首先需要定义autorun函数，接收update函数作为参数。因为调用autorun时要在Dep中注册订阅者，同时调用dep.notify()时要重新执行update函数，所以Dep中必须持有update引用，这里使用变量activeUpdate表示包裹update的函数。
实现代码如下
```js
let activeUpdate = null 

function autorun (update) {
  const wrappedUpdate = () => {
    activeUpdate = wrappedUpdate    // 引用赋值给activeUpdate
    update()                        // 调用update，即调用内部的dep.depend
    activeUpdate = null             // 绑定成功之后清除引用
  }
  wrappedUpdate()                   // 调用
}
```
wrappedUpdate本质是一个闭包，update函数内部可以获取到activeUpdate变量，同理dep.depend()内部也可以获取到activeUpdate变量，所以Dep的实现就很简单了。
实现代码如下
```js
class Dep {

  // 初始化
  constructor () {
    this.subscribers = new Set()
  }

  // 订阅update函数列表
  depend () {
    if (activeUpdate) {
      this.subscribers.add(activeUpdate)
    }
  }

  // 所有update函数重新运行
  notify () {
    this.subscribers.forEach(sub => sub())
  }
}
```
结合上面两部分就是完整实现。
#### 实例3：实现响应式系统
要求如下：
* 1、结合上述两个实例，convert()重命名为观察者observe()
* 2、observe()转换对象的属性使之响应式，对于每个转换后的属性，它会被分配一个Dep实例，该实例跟踪订阅update函数列表，并在调用setter时触发它们重新运行
* 3、autorun()接收update函数作为参数，并在update函数订阅的属性发生变化时重新运行。 
示例：
```js
const state = {
  count: 0
}

observe(state)

autorun(() => {
  console.log(state.count)
})
// 输出 count is: 0

state.count++
// 输出 count is: 1
```
结合实例1和实例2之后就可以实现上述要求，observe中修改obj属性的同时分配Dep的实例，并在get中注册订阅者，在set中通知改变。autorun函数保存不变。 实现如下：
```js
class Dep {

  // 初始化
  constructor () {          
    this.subscribers = new Set()
  }

  // 订阅update函数列表
  depend () {
    if (activeUpdate) {     
      this.subscribers.add(activeUpdate)
    }
  }

  // 所有update函数重新运行
  notify () {              
    this.subscribers.forEach(sub => sub())
  }
}

function observe (obj) {

  // 迭代对象的所有属性
  // 并使用Object.defineProperty()转换成getter/setters
  Object.keys(obj).forEach(key => {
    let internalValue = obj[key]

    // 每个属性分配一个Dep实例
    const dep = new Dep()

    Object.defineProperty(obj, key, {
    
      // getter负责注册订阅者
      get () {
        dep.depend()
        return internalValue
      },

      // setter负责通知改变
      set (newVal) {
        const changed = internalValue !== newVal
        internalValue = newVal
        
        // 触发后重新计算
        if (changed) {
          dep.notify()
        }
      }
    })
  })
  return obj
}

let activeUpdate = null

function autorun (update) {

  // 包裹update函数到"wrappedUpdate"函数中，
  // "wrappedUpdate"函数执行时注册和注销自身
  const wrappedUpdate = () => {
    activeUpdate = wrappedUpdate
    update()
    activeUpdate = null
  }
  wrappedUpdate()
}
```



## vue原理之插件
插件通常用来为 Vue 添加全局功能。
概括出来就是
* 1、通过Vue.use(MyPlugin)使用，本质上是调用MyPlugin.install(Vue)
* 2、使用插件必须在new Vue()启动应用之前完成，实例化之前就要配置好。
* 3、如果使用Vue.use多次注册相同插件，那只会注册成功一次。 

#### 源码解读
Vue.use源码如下
```js
Vue.use = function (plugin) {   
    // 忽略已注册插件
    if (plugin.installed) {
      return
    }
    
    // 集合转数组，并去除第一个参数(也就是plugin参数)
    var args = toArray(arguments, 1);
    
    // 把this（即Vue）添加到数组的第一个参数中(args包括vue和vueUse的第二个options参数)
    args.unshift(this);
    
    // 调用install方法
    if (typeof plugin.install === 'function') {
      plugin.install.apply(plugin, args);
    } else if (typeof plugin === 'function') {
      plugin.apply(null, args);
    }
    
    // 注册成功
    plugin.installed = true;
    return this;
  };
```
vue.use支持2个参数
> Vue.use(MyPlugin, { someOption: true })
vue.install调用可使用2个参数
> MyPlugin.install = function (Vue, options) {}

#### 例子
```js
MyPlugin.install = function (Vue, options) {
  // 1. 添加全局方法或属性
  Vue.myGlobalMethod = function () {
    // 逻辑...
  }

  // 2. 添加全局资源
  Vue.directive('my-directive', {
    bind (el, binding, vnode, oldVnode) {
      // 逻辑...
    }
    ...
  })

  // 3. 注入组件选项
  Vue.mixin({
    created: function () {
      // 逻辑...
    }
    ...
  })

  // 4. 添加实例方法
  Vue.prototype.$myMethod = function (methodOptions) {
    // 逻辑...
  }
}
```


## vueRender函数原理及实现

#### Actual DOM 和 Virtual DOM
Actual DOM 通过document.createElement('div')生成一个DOM节点。
```js
document.createElement('div')

// 浏览器原生对象（开销大）
"[object HTMLDivElement]"
```
Virtual DOM 通过 vm.$createElement('div')生成一个JS对象，VDOM对象有一个表示div的tag属性，有一个包含了所有可能特性的data属性，可能还有一个包含更多虚拟节点的children列表。
```js
vm.$createElement('div')

// 纯JS对象（轻量）
{ tag: 'div', data: { attrs: {}, ...}, children: [] }
```
因为Virtual DOM的渲染逻辑和Actual DOM解耦了，所以有能力运行在的非浏览器环境中，这就是为什么Virtual DOM出现之后混合开发开始流行的原因，React Native 和 Weex能够实现的原理就是这个。
#### JSX和Template
JSX和Template都是用于声明DOM和state之间关系的一种方式，在Vue中，Template是默认推荐的方式，但是也可以使用JSX来做更灵活的事。

JSX更加动态化，对于使用编程语言是很有帮助的，可以做任何事，但是动态化使得编译优化更加复杂和困难。

Template更加静态化并且对于表达式有更多约束，但是可以快速复用已经存在的模板，模板约束意味着可以在编译时做更多的性能优化，相对于JSX在编译时间上有着更多优势

#### createElement函数
```js
// @returns {VNode}
createElement(
  // {String | Object | Function}
  // 一个 HTML 标签字符串，组件选项对象，或者
  // 解析上述任何一种的一个 async 异步函数。必需参数。
  'div',

  // {Object}
  // 一个包含模板相关属性的数据对象
  // 你可以在 template 中使用这些特性。可选参数。
  {
    
  },

  // {String | Array}
  // 子虚拟节点 (VNodes)，由 `createElement()` 构建而成，
  // 也可以使用字符串来生成“文本虚拟节点”。可选参数。
  [
    '先写一些文字',
    createElement('h1', '一则头条'),
    createElement(MyComponent, {
      props: {
        someProp: 'foobar'
      }
    })
  ]
)
```
通常使用h作为createElement的别名，这是Vue的通用惯例，也是JSX的要求。


## jsx
JSX就是Javascript和XML结合的一种格式。React发明了JSX，利用HTML语法来创建虚拟DOM。当遇到<，JSX就当HTML解析，遇到{就当JavaScript解析.

#### 没了v-if,v-for,v-model怎么办？
* v-if
```js
  render(){
    return (
      <div>
        {this.show?'你帅':'你丑'}
      </div>
    )
  }
```
写三元表达式只能写简单的，那么复杂的还得用if/else
```js
  render(){
    let ifText
    if(this.show){
        ifText=<p>你帅</p>
    }else{
        ifText=<p>你丑</p>
    }
    return (
      <div>
        {ifText}
      </div>
    )
  }
```
* v-for
```js
  data(){
    return{
      show:false,
      list:[1,2,3,4]
    }
  },
  render(){
    return (
      <div>
        {this.list.map((v)=>{
          return <p>{v}</p>
        })}
      </div>
    )
  }
```
在jsx中｛｝中间是没办法写if/for语句的只能写表达式，所以就用map来当循环，用三元表达式来当判断了
* v-model
两个点：传值和监听事件改变值。
```js
<script>
  export default {
    name: "item",
    data(){
      return{
        show:false,
        list:[1,2,3,4],
        text:'',
      }
    },
    methods:{
      input(e){
        this.text=e.target.value
      }
    },
    render(){
      return (
        <div>
          <input type="text" value={this.text} onInput={this.input}/>
          <p>{this.text}</p>
        </div>
      )
    }
  }
</script>
```

#### 怎么用自定义组件？
很简单，只需要导入进来，不用再在components属性声明了，直接写在jsx中比如
```js
<script>
  import HelloWolrd from './HelloWorld'
  export default {
    name: "item",
    render(){
      return (
        <HelloWolrd/>
      )
    }
  }
</script>
```

#### 事件，class,style,ref等等怎么绑定？
```js
render (h) {
  return (
    <div
      // normal attributes or component props.
      id="foo"
      // DOM properties are prefixed with `domProps`
      domPropsInnerHTML="bar"
      // event listeners are prefixed with `on` or `nativeOn`
      onClick={this.clickHandler}
      nativeOnClick={this.nativeClickHandler}
      // other special top-level properties
      class={{ foo: true, bar: false }}
      style={{ color: 'red', fontSize: '14px' }}
      key="key"
      ref="ref"
      // assign the `ref` is used on elements/components with v-for
      refInFor
      slot="slot">
    </div>
  )
}
```


## 函数式组件
对于函数式组件，可以这样定义：
* Stateless(无状态)：组件自身是没有状态的
* Instanceless(无实例)：组件自身没有实例，也就是没有this 

#### Render context
* props
* children
* slots (a slots object)
* parent
* listeners
* injections
* data

其中上面的data包含了其他属性的引用

例子
```js
  <div id="app">
    <FunctionalButton id="aaa" aaa="123" @click="open"></FunctionButton>
  </div>

  Vue.component('functionalbutton', {
    name: 'funtional-button',
    functional: true,
    render(h, context) {
      console.log(contenx,contenx.slots().default)
      return h('button', contenx.data,['hello',contenx.slots().default])
    }
  })

  // 实例化
  new Vue({ el: '#app' ,methods: {
    open(){
      console.log('11111111111111')
    }
  }})
```

## js继承
#### 寄生组合式继承
结合借用构造函数传递参数和寄生模式实现继承
```js
//增强原型
function inheritPrototype(subType, superType){
  // Object.create创建一个空对象，这个空对象的原型式create函数的参数
  var prototype = Object.create(superType.prototype); // 创建对象，创建父类原型的一个副本
  prototype.constructor = subType;                    // 增强对象，弥补因重写原型而失去的默认的constructor 属性
  subType.prototype = prototype;                      // 指定对象，将新创建的对象赋值给子类的原型
}

// 父类初始化实例属性和原型属性
function SuperType(name){
  this.name = name;
  this.colors = ["red", "blue", "green"];
}
SuperType.prototype.sayName = function(){
  alert(this.name);
};

// 借用构造函数传递增强子类实例属性（支持传参和避免篡改）
function SubType(name, age){
  SuperType.call(this, name);
  this.age = age;
}

// 将父类原型指向子类
inheritPrototype(SubType, SuperType);

// 新增子类原型属性
SubType.prototype.sayAge = function(){
  alert(this.age);
}

var instance1 = new SubType("xyc", 23);
var instance2 = new SubType("lxy", 23);

instance1.colors.push("2"); // ["red", "blue", "green", "2"]
instance1.colors.push("3"); // ["red", "blue", "green", "3"]

```
这个例子的高效率体现在它只调用了一次SuperType 构造函数，并且因此避免了在SubType.prototype 上创建不必要的、多余的属性。于此同时，原型链还能保持不变；因此，还能够正常使用instanceof 和isPrototypeOf()
这是最成熟的方法，也是现在库实现的方法

#### ES6类继承extends
extends关键字主要用于类声明或者类表达式中，以创建一个类，该类是另一个类的子类。其中constructor表示构造函数，一个类中只能有一个构造函数，有多个会报出SyntaxError错误,如果没有显式指定构造方法，则会添加默认的 constructor方法，使用例子如下。extends继承的核心代码其实现和上述的寄生组合式继承方式一样
```js
class Rectangle {
    // constructor
    constructor(height, width) {
        this.height = height;
        this.width = width;
    }
    
    // Getter
    get area() {
        return this.calcArea()
    }
    
    // Method
    calcArea() {
        return this.height * this.width;
    }
}

const rectangle = new Rectangle(10, 20);
console.log(rectangle.area);
// 输出 200

-----------------------------------------------------------------
// 继承
class Square extends Rectangle {

  constructor(length) {
    super(length, length);// 子类的构造函数必须调用一次super()
    
    // 如果子类中存在构造函数，则需要在使用“this”之前首先调用 super()。
    this.name = 'Square';
  }

  get area() {
    return this.height * this.width;
  }
}

const square = new Square(10);
console.log(square.area);
// 输出 100

```

### 块级作用域

#### 暂时性死区
只要块级作用域内存在let命令，它所声明的变量就绑定这个区域，不再受外部影响
```js
var temp = 2;
if(true){
  temp = 4; //报错 referenceError
  let temp = 3;
}
```


### this的指向
1.在一般函数方法中使用 this 指代全局对象
```js
function test(){
　　　　this.x = 1;
　　　　alert(this.x);
　　}
　　test(); // 1
```

2.作为对象方法调用，this 指代上级对象
```js
function test(){
　　alert(this.x);
}
var o = {};
o.x = 1;
o.m = test;
o.m(); // 1
```

3.作为构造函数调用，this 指代new 出的对象
```js
function test(){
　　　　this.x = 1;
　　}
　　var o = new test();
　　alert(o.x); // 1
    //运行结果为1。为了表明这时this不是全局对象，我对代码做一些改变：
　　var x = 2;
　　function test(){
　　　　this.x = 1;
　　}
　　var o = new test();
　　alert(x); //2
```

4.apply call bind 调用 ，apply方法作用是改变函数的调用对象，此方法的第一个参数为改变后调用这个函数的对象，this指代第一个参数
```js
var x = 0;
　　function test(){
　　　　alert(this.x);
　　}
　　var o={};
　　o.x = 1;
　　o.m = test;
　　o.m.apply(); //0
//apply()的参数为空时，默认调用全局对象。因此，这时的运行结果为0，证明this指的是全局对象。如果把最后一行代码修改为

　　o.m.apply(o); //1
```

### 理解JavaScript 中的执行上下文和执行栈
执行上下文是当前 JavaScript 代码被解析和执行时所在环境的抽象概念。
执行上下文总共有三种类型:全局执行上下文,函数执行上下文,Eval 函数执行上下文
执行栈，也叫调用栈，具有 LIFO（后进先出）结构，用于存储在代码执行期间创建的所有执行上下文。
首次运行JS代码时，会创建一个全局执行上下文并Push到当前的执行栈中。每当发生函数调用，引擎都会为该函数创建一个新的函数执行上下文并Push到当前执行栈的栈顶。
根据执行栈LIFO规则，当栈顶函数运行完成后，其对应的函数执行上下文将会从执行栈中Pop出，上下文控制权将移到当前执行栈的下一个执行上下文。

执行上下文分两个阶段创建：1）创建阶段； 2）执行阶段
创建阶段包括 绑定this，创建词法环境和变量环境

This Binding
全局执行上下文中，this 的值指向全局对象，在浏览器中this 的值指向 window对象，而在nodejs中指向这个文件的module对象。
函数执行上下文中，this 的值取决于函数的调用方式。具体有：默认绑定、隐式绑定、显式绑定（硬绑定）、new绑定、箭头函数

词法环境（Lexical Environment）
词法环境包括2个组成部分为：环境记录：存储变量和函数声明的实际位置，对外部环境的引用：可以访问其外部词法环境
环境记录中记录了当前环境是全局环境还是函数环境，存储变量和函数声明的实际位置

变量环境也是一个词法环境，因此它具有上面定义的词法环境的所有属性。
变量环境只存储var，词法环境存储（let和const）和函数声明

例子
```js
let a = 20;  
const b = 30;  
var c;

function multiply(e, f) {  
 var g = 20;  
 return e * f * g;  
}

c = multiply(20, 30);


//全局执行上下文
GlobalExectionContext = {// 全局执行环境
  //this绑定全局
  ThisBinding: <Global Object>,

  //词法环境
  LexicalEnvironment: {  
    EnvironmentRecord: { //环境记录 
      Type: "Object",  // 全局环境
      // 标识符绑定在这里  
      a: < uninitialized >,  // a是let声明的所以是未初始化
      b: < uninitialized >,  
      multiply: < func >  // 函数声明
    }  
    outer: <null>  // 对外部环境的引用（因为是全局所以是null）
  },

  //变量环境
  VariableEnvironment: {  
    EnvironmentRecord: { //环境记录  
      Type: "Object",  
      // 标识符绑定在这里  
      c: undefined,  // c是var声明的所以是为undefined
    }  
    outer: <null>  
  }  
}

//函数执行上下文
FunctionExectionContext = {  
   
  //因为是默认绑定所以this绑定全局
  ThisBinding: <Global Object>,

  LexicalEnvironment: {  
    EnvironmentRecord: {  
      Type: "Declarative", // 函数环境 
      // 标识符绑定在这里  
      Arguments: {0: 20, 1: 30, length: 2},  
    },  
    outer: <GlobalLexicalEnvironment>   // 对外部环境的引用（可以使用全局的变量）
  },

  VariableEnvironment: {  
    EnvironmentRecord: {  
      Type: "Declarative",  
      // 标识符绑定在这里  
      g: undefined  
    },  
    outer: <GlobalLexicalEnvironment>  
  }  
}
```

### vo和ao
变量对象VO是与执行上下文相关的特殊对象,用来存储上下文的函数声明，函数形参和变量。
只有全局上下文的变量对象允许通过VO的属性名称来间接访问，在其他上下文(后面干脆直接讲函数上下文吧，我们并没有分析eval上下文)中是不能直接访问VO对象的。
在global全局上下文中，变量对象也是全局对象自身，在函数上下文中，变量对象被表示为活动对象AO（在函数执行上下文中，VO是不能直接访问的，此时由活动对象(activation object,缩写为AO)扮演VO的角色。）
如果变量名称跟已经声明的形式参数或函数相同，则变量声明不会干扰已经存在的这类属性。
```js
var a = 10;
function b () {
    console.log('全局的b函数')
};
function bar(a, b) {
    console.log('1', a, b) 
    var a = 1
    function b() {
        console.log('bar下的b函数')
    }
    console.log('2', a, b) 
}
bar(2, 3)
console.log('3', a, b)

// 首先分析上下文
// 全局上下文
// 创建阶段：
// 第一步，遇到了全局代码，进入全局上下文，此时的执行上下文栈是这样
ECStack = [
    globalContext: {
        VO: {
            // 根据1.2,会优先处理全局下的b函数声明,值为该函数所在内存地址的引用
            b: <reference to function>,
            // 紧接着，按顺序再处理bar函数声明，此时根据1.1，因为是在全局上下文中，并不会分析bar函数的参数
            bar: <refernce to function>,
            // 根据1.3,再处理变量，并赋值为undefined
            a: undefined
        }
    }
];
//第二步，发现bar函数被调用，就又创建了一个函数上下文，此时的执行上下文栈是这样
ECStack = [
    globalContext: {
        VO: {
            b: <reference to function b() {}>, 
            bar: <refernce to function bar() {}>,
            a: undefined
        }
    },
    <bar>functionContext: {
        VO: {
            // 根据1.1，优先分析函数的形参
            arguments: {
                0: 2,
                1: 3,
                length: 2,
                callee: bar
            },
            a: 2,
            // b: 3,
            // 根据1.2, 再分析bar函数中的函数声明b,并且赋值为b函数所在内存地址的引用, 它发现VO中已经有b:3了，就会覆盖掉它。因此上面一行中的b:3实际上不存在了。
            b: <refernce to function b() {}>
            // 根据1.3，接着分析bar函数中的变量声明a,并且赋值为undefined, 但是发现VO中已经有a:2了，因此下面一行中的a:undefined也是会不存在的。
            // a: undefined
        }
    }
]

以上就是执行上下文中的代码分析阶段，也就是执行上下文的创建阶段。再看看执行上下文的代码执行阶又发生了什么。
// 执行阶段：
// 第三步：首先，执行了bar(2, 3)函数，紧接着，在bar函数里执行了console.log('1', a, b)。全局上下文中依然还是VO，但是函数上下文中VO就变成了AO。并且代码执行到这，就已经修改了全局上下文中的变量a.
ECStack = [
    globalContext: {
        VO: {
            b: <reference to function b() {}>, 
            bar: <refernce to function bar() {}>,
            a: 10,
        }
    },
    <bar>functionContext: {
        AO: {
            arguments: {
                0: 2,
                1: 3,
                length: 2,
                callee: bar
            },
            a: 2,
            b: <refernce to function b() {}>
        }
    }
]

// 因此会输出结果： '1', 2, function b() {console.log('bar下的b函数')};

// 第四步：执行console.log('2', a, b)的时候, 发现里面的变量a被重新赋值为1了。
ECStack = [
    globalContext: {
        VO: {
            b: <reference to function b() {}>, 
            bar: <refernce to function bar() {}>,
            a: 10,
        }
    },
    <bar>functionContext: {
        AO: {
            arguments: {
                0: 2,
                1: 3,
                length: 2,
                callee: bar
            },
            a: 1,
            b: <refernce to function b() {}>
        }
    }
]
// 因此会输出结果： '2', 1, function b() {console.log('bar下的b函数')}；

// 第五步，执行到console.log('3', a, b)的时候，ECStack发现bar函数已经执行完了，就把bar从ECStack给弹出去了。此时的执行上下文栈是这样的。

ECStack = [
    globalContext: {
        VO: {
            b: <reference to function b() {}>, 
            bar: <refernce to function bar() {}>,
            a: 10,
        }
    }
]

// 因此会输出结果： '3', 10, function b() {console.log('全局的b函数')}

```


全局上下文的变量对象初始化是全局对象（其实这篇文章并没有介绍这个特性，不过它也很简单就这么一句话而已）
函数上下文的变量对象初始化只包括Arguments对象
在进入执行上下文的时候会给变量对象添加形参，函数声明，变量声明等初始的属性值
在代码执行阶段，会再次修改变量对象的属性值。

### 作用域
JavaScript采用的是词法作用域，函数的作用域基于函数创建的位置。
函数的作用域在函数定义的时候就决定了
这是因为函数有一个内部属性 [[scope]]，当函数创建的时候，就会保存所有父变量对象到其中，你可以理解 [[scope]] 就是所有父变量对象的层级链，但是注意：[[scope]] 并不代表完整的作用域链！

### 作用域链
这样由多个执行上下文的变量对象构成的链表就叫做作用域链。
作用域链是函数被创建的时候产生的

### 闭包
理论角度解释：
1、是一个函数
2、引用自由变量（自由变量为：不是自己的局部变量、不是参数）

实践角度解释：
1、即使创建它的上下文被弹出销毁了，该函数仍然存在（比如，内部函数从父函数中返回）
2、引用自由变量

### this绑定
this的绑定规则总共有下面5种。
1、默认绑定（严格/非严格模式）
2、隐式绑定
3、显式绑定
4、new绑定
5、箭头函数绑定

使用new来调用函数，或者说发生构造函数调用时，会自动执行下面的操作。
1、创建（或者说构造）一个新对象。
2、这个新对象会被执行[[Prototype]]连接。
3、这个新对象会绑定到函数调用的this。
4、如果函数没有返回其他对象，那么new表达式中的函数调用会自动返回这个新对象。

js赋值操作的返回值:赋什么值返回什么值

### 函数柯里化
实际上就是把add函数的x，y两个参数变成了先用一个函数接收x然后返回一个函数去处理y参数。现在思路应该就比较清晰了，就是只传递给函数一部分参数来调用它，让它返回一个函数去处理剩下的参数。
```js
function add(x, y) {
    return x + y
}

// Currying后
function curryingAdd(x) {
    return function (y) {
        return x + y
    }
}

add(1, 2)           // 3
curryingAdd(1)(2)   // 3
```

好处：
1、参数复用
```js
// 正常正则验证字符串 reg.test(txt)

// 函数封装后
function check(reg, txt) {
    return reg.test(txt)
}

check(/\d+/g, 'test')       //false
check(/[a-z]+/g, 'test')    //true

// Currying后
function curryingCheck(reg) {
    return function(txt) {
        return reg.test(txt)
    }
}

var hasNumber = curryingCheck(/\d+/g)
var hasLetter = curryingCheck(/[a-z]+/g)

hasNumber('test1')      // true
hasNumber('testtest')   // false
hasLetter('21212')      // false
```
2、提前确认
```js
var on = function(element, event, handler) {
    if (document.addEventListener) {
        if (element && event && handler) {
            element.addEventListener(event, handler, false);
        }
    } else {
        if (element && event && handler) {
            element.attachEvent('on' + event, handler);
        }
    }
}

var on = (function() {
    if (document.addEventListener) {
        return function(element, event, handler) {
            if (element && event && handler) {
                element.addEventListener(event, handler, false);
            }
        };
    } else {
        return function(element, event, handler) {
            if (element && event && handler) {
                element.attachEvent('on' + event, handler);
            }
        };
    }
})();

//换一种写法可能比较好理解一点，上面就是把isSupport这个参数给先确定下来了
var on = function(isSupport, element, event, handler) {
    isSupport = isSupport || document.addEventListener;
    if (isSupport) {
        return element.addEventListener(event, handler, false);
    } else {
        return element.attachEvent('on' + event, handler);
    }
}
```


### 内存泄漏
四种常见的JS内存泄漏
意外的全局变量
被遗忘的计时器或回调函数
脱离 DOM 的引用
闭包

undefined和null的区别
null表示此处不应该有值,undefined表示此处应有值但还没赋予，所以接触引用我们都用null而不用undefined


### LSH和RSH查询
LHS查询（左侧）：找到变量的容器本身，然后对其赋值（非严谨 非严格模式下如果没找到就定义为全局变量）
RHS查询（非左侧）：查找某个变量的值，可以理解为 retrieve his source value，即取到它的源值（较严谨 没找到就会抛出错误）

```js
function foo(a) {
  // a= 2 参数的隐式赋值触发LSH操作
  console.log( a ); // 2 // 查找console触发RSH 、查找a触发RSH
}

foo(2);// 函数调用的时候触发RSH查询
```

### set weakset map weakmap

set:
成员唯一无序且不重复
成员的格式为[value, value]，键值与键名是一致的（或者说只有键值，没有键名）类似于数组
可以遍历，方法有：add、delete、has
构造函数参数接受一个具有遍历器的任意参数（类似于数组）

weakset：
成员只能是对象，且成员都是弱引用，即垃圾回收后可能成员消失，所以不能遍历
不能遍历，方法有add、delete、has

map：
成员唯一无序且不重复
本质上是键值对的集合，类似集合
构造函数参数接受任何具有 Iterator 接口、且 每个成员都是一个双元素的数组 的数据结构
Map 的键实际上是跟内存地址绑定的，只要内存地址不一样，就视为两个键

weakmap：
键名只能是对象，而且是弱引用，键值可以是任意值，并且是正常引用
不能遍历，方法有add、delete、has

###文档的重绘和回流
回流：我们将可见DOM节点以及它对应的样式结合起来，可是我们还需要计算它们在设备视口(viewport)内的确切位置和大小，这个计算的阶段就是回流。
重绘：我们知道了哪些节点是可见的，以及可见节点的样式和具体的几何信息(位置、大小)，那么我们就可以将渲染树的每个节点都转换为屏幕上的实际像素，这个阶段就叫做重绘节点。
当页面布局和几何信息发生变化的时候，就需要回流
回流一定会触发重绘，而重绘不一定会回流

浏览器会将修改操作放入到队列里，直到过了一段时间或者操作达到了一个阈值，才清空队列。但是！当你获取布局信息的操作的时候，会强制队列刷新

减少重绘和回流：
避免触发同步布局事件
对于复杂动画效果,使用绝对定位让其脱离文档流
如果需要多次获取布局信息的时候（例如多次获取offsetWidth的时候）最好保存到变量中，避免重复引发回流
### vuex
