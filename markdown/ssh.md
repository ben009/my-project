## window生成查看ssh key

第一步 查看是否存在ssh

>cd ~/.ssh/

如果提示No such file or directory代表没有创建过,手动创建

>mkdir ~/.ssh 

配置全局的name和email，这里是的你github的name和email

生成key
>ssh-keygen -t rsa -C "youremail@163.com"

查看key

>cd ~/.ssh/
cat id_rsa.pub

## mac生成查看ssh key

第一步 查看是否存在ssh
>ssh -v

如果没有安装 执行
>ssh-keygen -t rsa -C "youremail@example.com"

查看ssh
>ls -a ~/.ssh
vim ~/.ssh/id_rsa.pub


