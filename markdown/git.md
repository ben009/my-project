### 创建分支
>git checkout -b 分支名称

### 查看分支
>git branch

### 切换分支
>git checkout 分支名称

### 查看提交记录
>git log

### 将工作去的内容放入版本库的暂存区
>git add . // git add 文件名

### 将暂存区的代码提交到当前分支
>git commit -m 注释

### 克隆项目
>git clone

### 推送分支
>git push origin gitLearn

### 拉取分支
>git pull

### 用于合并指定分支到当前分支
>git merge dev

### 删除本地分支
>git branch -d dev

### 删除远程分支
>git push origin :dev